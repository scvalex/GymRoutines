package org.abstractbinary.gymrat.ui.settings.general

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import org.abstractbinary.gymrat.data.resetAppSettings
import kotlinx.coroutines.launch

class GeneralSettingsViewModel(
    private val preferences: DataStore<Preferences>
) : ViewModel() {
    fun resetSettings() {
        viewModelScope.launch {
            preferences.resetAppSettings()
        }
    }
}
