package org.abstractbinary.gymrat.ui.theme

import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import org.abstractbinary.gymrat.R

val RobotoSlab = FontFamily(
    Font(R.font.roboto_slab_black, FontWeight.W900)
)
