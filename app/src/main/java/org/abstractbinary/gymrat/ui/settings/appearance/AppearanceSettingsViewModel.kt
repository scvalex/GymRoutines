package org.abstractbinary.gymrat.ui.settings.appearance

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import org.abstractbinary.gymrat.data.AppPrefs
import org.abstractbinary.gymrat.data.ColorTheme
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

class AppearanceSettingsViewModel(
    private val preferences: DataStore<Preferences>
) : ViewModel() {
    val appTheme = preferences.data.map {
        it[AppPrefs.AppTheme.key]?.let { colorThemeString ->
            ColorTheme.valueOf(colorThemeString)
        } ?: ColorTheme.FollowSystem
    }

    fun setAppTheme(theme: ColorTheme) {
        viewModelScope.launch {
            preferences.edit {
                it[AppPrefs.AppTheme.key] = theme.name
            }
        }
    }
}
