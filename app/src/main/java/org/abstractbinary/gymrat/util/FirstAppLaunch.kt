package org.abstractbinary.gymrat.util

import android.content.Context
import org.abstractbinary.gymrat.data.AppPrefs
import org.abstractbinary.gymrat.data.datastore
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext

suspend fun Context.isFirstRun(): Boolean = withContext(Dispatchers.Default) {
    datastore.data.map { it[AppPrefs.IsFirstRun.key] ?: true }.first()
}
