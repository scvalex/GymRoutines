package org.abstractbinary.gymrat.util

import java.text.DateFormat
import java.util.*
import kotlin.time.Duration

fun Date.formatSimple(): String {
    return DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(this)
}

fun Duration.pretty(): String = toComponents { h, m, _, _ -> "${h}h ${m}min" }

fun Duration.iso8601(): String = toComponents { h, m, s, _ ->
    String.format("%02d:%02d:%02d", h, m, s)
}
