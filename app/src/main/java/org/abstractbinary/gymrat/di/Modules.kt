/*
 * Splitfit
 * Copyright (C) 2020  Noah Jutz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.abstractbinary.gymrat.di

import androidx.room.Room
import org.abstractbinary.gymrat.data.*
import org.abstractbinary.gymrat.ui.exercises.editor.ExerciseEditorViewModel
import org.abstractbinary.gymrat.ui.exercises.list.ExerciseListViewModel
import org.abstractbinary.gymrat.ui.exercises.picker.ExercisePickerViewModel
import org.abstractbinary.gymrat.ui.main.MainScreenViewModel
import org.abstractbinary.gymrat.ui.routines.editor.RoutineEditorViewModel
import org.abstractbinary.gymrat.ui.routines.list.RoutineListViewModel
import org.abstractbinary.gymrat.ui.settings.appearance.AppearanceSettingsViewModel
import org.abstractbinary.gymrat.ui.settings.data.DataSettingsViewModel
import org.abstractbinary.gymrat.ui.settings.general.GeneralSettingsViewModel
import org.abstractbinary.gymrat.ui.workout.completed.WorkoutCompletedViewModel
import org.abstractbinary.gymrat.ui.workout.in_progress.WorkoutInProgressViewModel
import org.abstractbinary.gymrat.ui.workout.insights.WorkoutInsightsViewModel
import org.abstractbinary.gymrat.ui.workout.viewer.WorkoutViewerViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val koinModule = module {
    single {
        Room.databaseBuilder(androidContext(), AppDatabase::class.java, "workout_routines_database")
            .addMigrations(
                MIGRATION_36_37,
                MIGRATION_37_38,
                MIGRATION_38_39,
                MIGRATION_39_40,
                MIGRATION_40_41,
                MIGRATION_41_42,
                MIGRATION_42_43,
            )
            .build()
    }

    factory {
        androidContext().datastore
    }

    factory {
        get<AppDatabase>().exerciseDao
    }

    factory {
        get<AppDatabase>().routineDao
    }

    factory {
        get<AppDatabase>().workoutDao
    }

    factory {
        WorkoutRepository(workoutDao = get())
    }

    factory {
        RoutineRepository(routineDao = get())
    }

    factory {
        ExerciseRepository(exerciseDao = get())
    }

    viewModel {
        RoutineListViewModel(get())
    }

    viewModel {
        ExerciseListViewModel(get())
    }

    viewModel {
        ExercisePickerViewModel(exerciseRepository = get())
    }

    viewModel { params ->
        ExerciseEditorViewModel(repository = get(), exerciseId = params.get())
    }

    viewModel { params ->
        RoutineEditorViewModel(
            preferences = get(),
            exerciseRepository = get(),
            routineRepository = get(),
            workoutRepository = get(),
            routineId = params.get()
        )
    }

    viewModel { params ->
        WorkoutInProgressViewModel(
            preferences = get(),
            workoutRepository = get(),
            exerciseRepository = get(),
            routineRepository = get(),
            application = androidApplication(),
            workoutId = params.get(),
        )
    }

    viewModel {
        WorkoutInsightsViewModel(
            workoutRepository = get(),
            routineRepository = get(),
            preferences = get(),
        )
    }

    viewModel { params ->
        WorkoutViewerViewModel(
            workoutId = params.get(),
            workoutRepository = get(),
            exerciseRepository = get(),
            routineRepository = get(),
        )
    }

    viewModel {
        MainScreenViewModel(
            preferences = get()
        )
    }

    viewModel {
        AppearanceSettingsViewModel(
            preferences = get()
        )
    }

    viewModel {
        DataSettingsViewModel(
            database = get(),
            application = androidApplication(),
            preferences = get(),
        )
    }

    viewModel {
        GeneralSettingsViewModel(
            preferences = get()
        )
    }

    viewModel { params ->
        WorkoutCompletedViewModel(
            workoutId = params[0],
            routineId = params[1],
            preferences = get(),
            routineRepository = get(),
            workoutRepository = get(),
        )
    }
}
