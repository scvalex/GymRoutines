{ pkgs ? import <nixpkgs-unstable> { } }:
pkgs.mkShell {
  nativeBuildInputs = with pkgs; [
    android-studio
    android-tools
    tokei
  ];
}
